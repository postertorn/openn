# -*- coding: utf-8 -*-

class Version:
  VERSION = 'v1.16.0'

  @classmethod
  def version(cls):
      return cls.VERSION
