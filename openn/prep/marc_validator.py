# -*- coding: utf-8 -*-
from lxml import etree

class MarcValidator(object):
    """For the provided set of requirements validate a give MARC 21 XML file."""
    def __init__(self, xml_io, required_xpaths = None, holdings_id = None):
        super(MarcValidator, self).__init__()
        self.tree = etree.parse(xml_io)
        self.required_xpaths = required_xpaths
        self.holdings_id = holdings_id
        self.namespaces = { 'marc': 'http://www.loc.gov/MARC21/slim' }
        self.errors = []

    def validate(self):
        del self.errors[:] # clear the errors array

        self.validate_shelfmark()
        self.validate_required_xpaths()
        return len(self.errors) == 0

    def validate_required_xpaths(self):
        if self.required_xpaths is None:
            return

        for xpath in self.required_xpaths:
            if len(self.tree.xpath(xpath, namespaces=self.namespaces)) < 1:
                self.errors.append("Required XML not found: '%s'" % (xpath,))

        self.validate_shelfmark()

    def validate_shelfmark(self):
        # we have a holdings ID
        if self.holdings_id is not None:
            xpath = "//marc:holding_id[text() = '%s']/parent::marc:holding/marc:call_number/text()" % self.holdings_id
            call_numbers = self.tree.xpath(xpath, namespaces=self.namespaces)
            if len(call_numbers) == 1:
                return
            self.errors.append('Expected 1 call number for holding ID %s; found %d in Penn MARC XML' % (self.holdings_id, len(call_numbers)))


        # No holdings ID; let's check for a holdings record
        xpath = "//marc:holding/marc:call_number/text()"
        call_numbers = self.tree.xpath(xpath, namespaces=self.namespaces)
        if len(call_numbers) > 1:
            self.errors.append('Please provide holding ID; more than one call number found in MARC XML')
            return
        # Only one call number; no error; return
        if len(call_numbers) == 1:
            return

        call_numbers = []
        # Look for a Marc record with 500$a 'Shelfmark:'
        xpath = "//marc:record/marc:datafield[@tag='500']/marc:subfield[@code='a' and starts-with(text(), 'Shelfmark:')]"
        call_numbers += self.tree.xpath(xpath, namespaces=self.namespaces)
        if len(call_numbers) == 1:
            return

        # Try the 099$a subfield
        xpath = "//marc:record/marc:datafield[@tag='099']/marc:subfield[@code='a']"
        call_numbers += self.tree.xpath(xpath, namespaces=self.namespaces)
        if len(call_numbers) == 1:
            return

        # Check for Princeton call number in 852 field with 852$b == 'hsvm'
        xpath = "//marc:record/marc:datafield[@tag='852' and marc:subfield[@code='b']/text() = 'hsvm']/marc:subfield[@code='h'][1]"
        call_numbers += self.tree.xpath(xpath, namespaces=self.namespaces)
        if len(call_numbers) == 1:
            return

        if len(call_numbers) < 1:
            self.errors.append('No call number found in in MARC XML')
        elif len(call_numbers) > 1:
            self.errors.append('Please provide holding ID; more than one call number found in MARC XML')

