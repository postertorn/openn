# -*- coding: utf-8 -*-

import os
import re
import logging
from copy import deepcopy

from django.conf import settings

class Assets(object):

    def build_paths(self):
        """Create a list of tuples of '(source_file, dest_file)' paths for every asset."""
        source_dests = []

        for source_path, rel_path in self.source_paths():
            dest = os.path.join(settings.SITE_DIR, settings.ASSETS_DIR, rel_path)
            source_dests.append((source_path, dest))

        return source_dests

    def assets_dirs(self):
        """These are the template dirs containing assets directories."""
        # there's only one of these, but TEMPLATE_DIRS is a tuple so thie method deals with that
        dirs = [os.path.join(d, 'assets') for d in settings.TEMPLATE_DIRS]
        # => ['/path/to/openn/templates/assets']
        return [d for d in dirs if os.path.exists(d)]

    def source_paths(self):
        """Return a list of tuples of each full path to an asset and its path relative to
        the 'assets' folder; e.g.,

            [('/path/to/openn/templates/assets/xlsx/pages.xlsx', 'xlsx/pages.xlsx'), ...]
        """
        source_dirs = self.assets_dirs()

        assets = []
        for source_dir in source_dirs:
            pattern = r'^%s/?' % source_dir
            for root, dirs, files in os.walk(source_dir):
                for f in files:
                    if not f.startswith('.'): # skip hidden files like .DS_Store
                        source = os.path.join(root, f)
                        rel = re.sub(pattern, '', source)
                        assets.append((source, rel))

        return assets
