{% extends "base.html" %}

{% block title %}Contributing to OPenn{% endblock %}

{% block content %}

<div class="breadcrumb"><p>
    <span><a href="/">Home</a>&nbsp;</span>
</p></div>


{% load markdown_deux_tags %}
{% markdown %}

# Contributing to OPenn

## What is OPenn?

OPenn is a website that hosts manuscript images and metadata from the University of Pennsylvania Libraries and collaborating institutions as open data for bulk download and reuse. The images and metadata on OPenn are free to download and reuse under Creative Commons licenses approved for [Free Cultural Works][FreeWorks]; that is, these materials may be shared and reused in any form and for any purpose, including commercial uses.

[FreeWorks]: https://creativecommons.org/share-your-work/public-domain/freeworks/ "Understanding Free Cultural Works"

The images available for each object include high-resolution archival TIFFs and JPEGs as thumbnails and in a larger format suitable for online use. The metadata available are TEI P5 XML manuscript descriptions, MARC XML, and technical image metadata.

### What OPenn is not

OPenn is not designed for preservation. It is designed to facilitate access and reuse. We do make backup copies of the data on OPenn, but OPenn does not provide the functions or services of a digital repository.

OPenn is not a manuscript viewing or discovery interface. Think of OPenn as providing "open-stack" access to the digital files.

## Why contribute?

If you or your institution would like to open up your manuscript images and metadata to the public for download and reuse, then OPenn could be a good fit. OPenn has aggregated manuscript data from over 50 repositories and collections in Philadelphia, the mid-Atlantic region and beyond, thanks substantially to material digitized by several preservation and access grants from the [Council on Library and Information Resources][CLIR] and the [National Endowment for the Humanities][NEH]. Several CLIR-funded preservation and access projects use OPenn as a data access portal. These grants and other projects with Penn's partners have made OPenn a major hub for open manuscript data. OPenn hosts images and metadata for over 10,000 manuscript objects with over one million page unique images, amounting to 88 TB of data. OPenn has strong collections of digitized medieval, Islamicate, Hebrew and early Philadelphia material. For more information on OPenn's collections and Collection Policy see [OPenn's Collection Policy][Collecting].


[CLIR]:       https://www.clir.org    "Council on Library and Information Resources"
[NEH]:        https://www.neh.gov     "National Endowment for the Humanities"
[Collecting]: /CollectionPolicy.html  "OPenn's Collection Policy"

## Is OPenn a good fit?

Most projects to add data to OPenn are sponsored by the curators of the Kislak Center for Special Collections, Rare Books and Manuscripts and the Schoenberg Institute for Manuscript Studies at Penn Libraries. If you're not sure who to contact, you can get in touch with the OPenn team, Doug Emery and Jessie Dummer, at <openn@pobox.upenn.edu>.

We can discuss your project, its fit for OPenn, and our requirements and process for adding new data. If OPenn is a good fit for your data, we can help to identify the best data delivery methods and workflows for your project.

## How does it work?


We work with you to set up a landing page for each repository, identify licenses, and plan for the preparation and delivery of your data and metadata. On some projects we may be able to work with curators at Penn Libraries to arrange digitization and metadata collection for your materials.

### Repository page setup

If we don't already have one for your organization or collection, we will work with you to create a splash page for your repository on OPenn with a description that includes an introduction to your collections and how your material is licensed on OPenn. Here's an example of the Science History Institute's repository page: <https://openn.library.upenn.edu/html/0025.html>.

### Licenses

Contributors to OPenn need to select a rights designation for their materials on OPenn. OPenn uses the [CreativeCommons.org][CC] designations they deem "Approved for Free Cultural Works". We use these two marks (PD, CC0) and two licences (CC-BY, CC-BY-SA) for objects on OPenn.

[CC]: https://creativecommons.org "Creative Commons"

- [Public Domain (PD)][PD] -- This mark designates items in the public domain that are freely and openly available to the public because they are not restricted under copyright law.
- [CC0 (i.e. "CC zero")][CC0] --  This mark designates items for which an owner relinquishes copyright. Users may download and reuse items under this license as if they were in the public domain.
- [CC-BY][CC-BY] -- This license designates items that users may download, use and reuse as long as credit is given to the copyright holder.
- [CC-BY-SA][CC-BY-SA] -- This license designates items that users may download, use and reuse as long as attribution is given to the copyright holder (as in the CC-BY license), and as long as any adaptations or changes made to the original work are made available under a CC-BY-SA license as well.

[PD]:       https://creativecommons.org/share-your-work/public-domain/pdm/  "Public Domain Mark"
[CC0]:      https://creativecommons.org/share-your-work/public-domain/cc0/  "CC0"
[CC-BY]:    https://creativecommons.org/licenses/by/4.0/                    "CC-BY Attribution license"
[CC-BY-SA]: https://creativecommons.org/licenses/by-sa/4.0/                 "CC-BY-SA Attribution-ShareAlike license"

It is important to note that none of these licenses prohibit commercial use of materials or the creation of derivatives.

### Data preparation

OPenn typically accepts hard drives of TIFF images along with accompanying descriptive and structural metadata for each item. Descriptive metadata can be either in MARC XML format or in spreadsheets formatted for OPenn ingestion. We accept structural (page-level) metadata in a specially-formatted spreadsheet. Here is an example of a spreadsheet that provides both descriptive and structural metadata in different tabs: [openn_metadata.xlsx][openn_metadata]. Here is an example of a structural metadata spreadsheet that would accompany a MARC XML record with descriptive metadata: [pages.xlsx][pages_xlsx].

[openn_metadata]:  /html/xlsx/openn_metadata.xlsx "OPenn descriptive and structural metadata spreadsheet"
[pages_xlsx]:      /html/xlsx/pages.xlsx          "OPenn structural metadata spreadsheet"

Don't have images of your materials? We may be able to help. Get in touch with us at <openn@pobox.upenn.edu>. For a number of projects Penn Libraries' curators in the Kislak Center for Special Collections, Rare Books & Manuscripts have arranged for imaging and metadata description for repositories outside the Penn Libraries.

Again, if you do contribute to OPenn, we will work with you to identify the best data preparation and delivery methods for your project and your data. We also provide guidance in the collection and preparation of data and metadata for submission.

{% endmarkdown %}
{% endblock %}