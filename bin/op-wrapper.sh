#!/usr/bin/env bash

################################################################################
#
# Generic wrapper for all OPenn python scripts.
#
# Collects the command (as -C SCRIPT.py); activates the virtualenv;
# runs the script; deactivates the virtualenv; returns the exit status
# of the called python script.
#
################################################################################

usage() {
    echo "Usage: `basename $0` SCRIPT.py ARGS"
    echo ""
    echo "Options"
    echo ""
    echo " -C <SCRIPT.py>   Name of the Python script to execute"
    echo
}

# SCRIPT NAME CHECK
# Do we have an arg?
if [ $# -lt 2 ]; then
    echo "Please provide a Python script path"
    usage
    exit 1
fi

if [ "$1" = "-C" ]; then
    :
else
    echo "Please provide a Python script path using the -C flag"
    usage
    exit 1
fi
shift

# Does the file exist?
if [ -f "$1" ]; then
    SCRIPT_PY="$1"
else
    echo "ERROR: Could not find Python script: $1"
    usage
    exit 1
fi

# shift arguments
shift

this_dir=`dirname $0`
venv_dir=${this_dir}/../venv
python_version=${this_dir}/../.python-version

if [ -d $venv_dir ]; then
    # Virtualenv?
    echo "INFO: Using virtualenv dir ${venv_dir}"
    source ${venv_dir}/bin/activate
    deactivate_after_finish=true
elif which pyenv && [ -f ${python_version} ]; then
    echo "INFO: Using pyenv and python verion $(cat ${python_version}) from '${python_version}'"
else
    echo "WARNING: no virtualenv dir (${venv_dir}) or pyenv version (${python_version}) found; running anyway"
fi


python $SCRIPT_PY $@
status=$?

if [ -n "$deactivate_after_finish" ]; then
    deactivate
fi

exit $status
